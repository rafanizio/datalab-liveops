def drop_orders():
    return """
    drop table if exists  liveops_orders cascade;
    """


def drop_drivers():
    return """
    drop table if exists  liveops_drivers cascade;
        """


def drop_saturacao_drivers():
    return """
    drop table if exists  liveops_saturaca_drivers cascade;
        """


def drop_tuktuk():
    return """
    drop table if exists  liveops_tuktuk cascade;
        """


def drop_apinova():
    return """
    drop table if exists  liveops_api_nova cascade;
        """


def drop_label():
    return """
    drop table if exists  label_language cascade;
        """
