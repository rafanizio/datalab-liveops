




def usa():
    return """
INSERT INTO public.label_language (id, label_languages, global_title_orders, global_title_foods_kpi, global_title_drivers, 
    global_title_sellers, global_box_total_orders, global_box_total_orders_d7, global_box_oct, global_box_critical_orders_oct, 
    global_box_ttest, global_box_critical_orders_tt_est, global_box_active_drivers, global_box_active_drivers_d7, global_box_drivers_onshift, 
    global_box_drivers_occupation_rate, global_box_active_sellers, global_box_active_sellers_d7, global_table_stations, global_graph1_accum, 
    global_graph2_allstatus, global_graph3_statuscreated, global_graph4_statusready, global_graph5_statusready, global_box_latest_update) 
VALUES (1, 'English', 'Orders', 'KPIs - FOODS', 'Drivers', 'Sellers', 'Total orders today', 'Variation vs. D-7', 'Avg OCT', 
    'Critical orders OCT', 'Avg TT + EST', 'Critical orders TT + EST', 'Current active drivers', 'Variation vs. D-7', 'Drivers on shift',
    'Driver occupation rate', 'Current active sellers', 'Variation vs. D-7', 'KPIs by station', 'Accum. total orders today',
    'Order status growth - current day', 'Created orders comparison', 
    'Under production orders comparison', 'In expedition orders comparison', 'Latest update'); """







def br():
return """
INSERT INTO public.label_language (id, label_languages, global_title_orders, global_title_foods_kpi, global_title_drivers, global_title_sellers, global_box_total_orders,
         global_box_total_orders_d7, global_box_oct, global_box_critical_orders_oct, global_box_ttest, global_box_critical_orders_tt_est,
         global_box_active_drivers, global_box_active_drivers_d7, global_box_drivers_onshift, global_box_drivers_occupation_rate, global_box_active_sellers,
         global_box_active_sellers_d7, global_table_stations, global_graph1_accum, global_graph2_allstatus, global_graph3_statuscreated, global_graph4_statusready,
         global_graph5_statusready, global_box_latest_update)
 VALUES (2, 'Português', 'Pedidos', 'Indicadores FOODS', 'Entregadores', 'Lojistas', 'Total de pedidos hoje', 'Variação vs. D-7', 
        'OCT médio', 'Pedidos críticos OCT', 'TT + EST médio', 'Pedidos críticos TT + EST', 'Entregadores ativos hoje', 'Variação vs. D-7', 
        'Entregadores no turno', 'Taxa de ocupação', 'Lojistas ativos hoje', 'Variação vs. D-7', 'Abertura por station', 
        'Acumulado - Total de pedidos hoje', 'Evolução dos status dos pedidos hoje', 'Comparação dos pedidos criados', 
        'Comparação dos pedidos em produção', 'Comparação dos pedidos em expedição', 'Última atualização');
"""