from dao import *
from querys_incremen import query_orders_inc
import time
from sqlalchemy import create_engine

bol_day = True


def eng_master():
    conn = engine_master()
    eng_master = conn.connect()
    return eng_master


def run_orders():
    global bol_day
    eng_master_conn = eng_master()
    creation_date = eng_master_conn.execute(
        "select date(current_timestamp at time zone 'America/Sao_Paulo');"
    ).fetchall()
    eng_master_conn.close()
    while bol_day == True:
        eng_master_conn = eng_master()
        creation_max = eng_master_conn.execute(
            "select max(timezone('UTC'::text, order_created_at_ms)) from liveops_orders;"
        ).fetchall()
        df_orders = query_get_replica(query_orders_inc(str(creation_max[0][0])))
        if len(df_orders) > 0:
            df_orders.reset_index(drop=True, inplace=True)
            query_insert_master(dataframe=df_orders, table_destiny="liveops_orders")
            print("Novos Pedidos desde: :", creation_max)
        else:
            print("Sem novos pedidos desde:", creation_max)
            time.sleep(60)
        eng_master_conn.execute(
            "refresh materialized view concurrently mvw_liveops_orders;"
        )
        eng_master_conn.execute(
            "refresh materialized view concurrently mvw_liveops_status_histories;"
        )
        same_day = eng_master_conn.execute(
            "select date(current_timestamp at time zone 'America/Sao_Paulo');"
        ).fetchall()
        if creation_date == same_day:
            eng_master_conn.close()
            time.sleep(30)
            bol_day = True
        else:
            print("Encerrando o dia  - ORDERS")
            eng_master_conn.close()
            bol_day = False


if __name__ == "__main__":

    run_orders()
