from dao import *
from querys_incremen import query_drivers
import time
from sqlalchemy import create_engine

bol_day = True


def eng_master():
    conn = engine_master()
    eng_master = conn.connect()
    return eng_master


def run_drivers():
    global bol_day
    eng_master_conn = eng_master()
    creation_date = eng_master_conn.execute(
        "select date(current_timestamp at time zone 'America/Sao_Paulo');"
    ).fetchall()
    eng_master_conn.close()
    while bol_day == True:
        eng_master_conn = eng_master()
        same_day = eng_master_conn.execute(
            "select date(current_timestamp at time zone 'America/Sao_Paulo');"
        ).fetchall()
        creation_max = eng_master_conn.execute(
            "select max(driver_creation_ms) from liveops_drivers"
        ).fetchall()
        df_drivers = query_get_replica(query_drivers(str(creation_max[0][0])))
        if len(df_drivers) > 0:
            df_drivers.reset_index(drop=True, inplace=True)
            query_insert_master(dataframe=df_drivers, table_destiny="liveops_drivers")
            print(" Novos DRIVERS Atualizada as :", creation_max)
            eng_master_conn.execute(
                "refresh materialized view concurrently  public.mvw_liveops_drivers"
            )
        else:
            print("Sem novos DRIVERS desde : ", creation_max)
            time.sleep(10)
        if creation_date == same_day:
            eng_master_conn.close()
            time.sleep(4)
            bol_day = True
        else:
            print("Encerrando Drivers")
            eng_master_conn.close()
            bol_day = False


if __name__ == "__main__":
    run_drivers()
