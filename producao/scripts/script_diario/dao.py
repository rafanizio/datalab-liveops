import psycopg2
import pandas as pd
from sqlalchemy import create_engine


def conn_replica_liveops():
    conn_replica = psycopg2.connect(
        host="",
        database="deliverycenter_api",
        port="",
        user="",
        password="",
    )
    return conn_replica


def conn_replica_tuktuk():
    conn_replica_tuktuk = psycopg2.connect(
        host="",
        database="tuktuk-prod",
        port="",
        user="",
        password="",
    )

    return conn_replica_tuktuk


def engine_master():
    return create_engine("postgresql://postgres:senha@35.245.219.251:5432/postgres")


def query_get_replica(query):
    conn_repl = conn_replica_liveops()
    cursor = conn_repl.cursor()
    cursor.execute("set timezone = 'America/Sao_Paulo'")
    df = pd.read_sql_query(query, conn_repl)
    conn_repl.close()
    return df


def query_get_replica_tuktuk(query):
    conn_repl_tuk = conn_replica_tuktuk()
    df_tuktuk = pd.read_sql_query(query, conn_repl_tuk)
    conn_repl_tuk.close()
    return df_tuktuk


def query_get_master_liveops(query):
    conn_master = psycopg2.connect(
        host="",
        database="postgres",
        port="",
        user="",
        password="",
    )
    df = pd.read_sql_query(query, conn_master)
    conn_master.close()
    return df


def query_insert_master(dataframe, table_destiny):
    engine = engine_master()
    engine.execute("set timezone = 'America/Sao_Paulo'")
    dataframe.to_sql(
        table_destiny,
        con=engine,
        schema="public",
        index=False,
        if_exists="append",
        method="multi",
    )
