import pandas as pd
import time
from querys import query_pedidos_drivers, query_sla
from dao import *

bol_day = True


def saturacao():
    df_pedidos_drivers = query_get_master_liveops(query_pedidos_drivers())
    df_slas = query_get_master_liveops(query_sla())
    if len(df_pedidos_drivers) == 0 or len(df_slas) == 0:
        df = pd.DataFrame(columns=["empty"])
        return df

    else:

        def calc_taxa_ocupacao(at_home, on_shift):
            if on_shift == 0:
                return 0

            else:
                return 1 - (round((at_home / on_shift), 4))

        df_pedidos_drivers["taxa_ocupacao"] = df_pedidos_drivers.apply(
            lambda row: calc_taxa_ocupacao(row.at_home, row.on_shift), axis=1
        )

        # Calcula pedidos em produção por driver

        def calc_producao_driver(pedidos_producao, n_drivers):
            if n_drivers == 0:
                return pedidos_producao

            else:
                return round((pedidos_producao / n_drivers), 4)

        df_pedidos_drivers["producao_por_drivers"] = df_pedidos_drivers.apply(
            lambda row: calc_producao_driver(row.pedidos_producao, row.at_home), axis=1
        )

        # Calcula pedidos em expedição por driver

        def calc_expedicao_driver(pedidos_expedicao, n_drivers):
            if n_drivers == 0:
                return pedidos_expedicao

            else:
                return round((pedidos_expedicao / n_drivers), 4)

        df_pedidos_drivers["expedicao_por_drivers"] = df_pedidos_drivers.apply(
            lambda row: calc_expedicao_driver(row.pedidos_expedicao, row.at_home),
            axis=1,
        )

        # Calcula pedidos criticos

        def check_critico(tt, est):
            tt_est = tt + est

            if tt_est >= 40:
                return 1
            else:
                return 0

        df_slas["pedido_critico"] = df_slas.apply(
            lambda row: check_critico(row.tt, row.est), axis=1
        )

        df_critico = pd.DataFrame(
            df_slas.groupby("hub_id")["pedido_critico"].agg(["count", "sum"])
        ).reset_index()

        df_critico.columns = ["hub_id", "n_orders", "n_criticos"]

        def calc_criticos(n_criticos, n_orders):
            if n_orders == 0:
                return 0
            else:
                return round((n_criticos / n_orders), 4)

        df_critico["percent_criticos"] = df_critico.apply(
            lambda row: calc_criticos(row.n_criticos, row.n_orders), axis=1
        )

        # Calcula Medianas

        df_median = pd.DataFrame(
            df_slas.groupby("hub_id")[["pt", "est", "tt"]].median()
        ).reset_index()

        # Join de todas as variáveis

        df = pd.DataFrame()

        df = df_pedidos_drivers[
            ["hub_id", "taxa_ocupacao", "producao_por_drivers", "expedicao_por_drivers"]
        ].copy()

        df = df.join(
            df_critico[["hub_id", "percent_criticos"]].set_index("hub_id"), on="hub_id"
        )

        df = df.join(df_median.set_index("hub_id"), on="hub_id")

        # Calcula scores

        def get_score_taxa_ocupacao(taxa_ocupacao):
            if taxa_ocupacao < 0.1:
                return 0
            elif taxa_ocupacao < 0.4:
                return 5
            elif taxa_ocupacao < 0.6:
                return 7
            elif taxa_ocupacao < 0.8:
                return 8
            elif taxa_ocupacao < 0.9:
                return 9
            else:
                return 10

        df["score_taxa_ocupacao"] = df.apply(
            lambda row: get_score_taxa_ocupacao(row.taxa_ocupacao), axis=1
        )

        def get_score_producao_por_drivers(producao_por_drivers):
            if producao_por_drivers < 1:
                return 0
            elif producao_por_drivers < 2:
                return 2
            elif producao_por_drivers < 3:
                return 6
            elif producao_por_drivers < 4:
                return 8
            elif producao_por_drivers < 5:
                return 9
            else:
                return 10

        df["score_producao_por_drivers"] = df.apply(
            lambda row: get_score_producao_por_drivers(row.producao_por_drivers), axis=1
        )

        def get_score_expedicao_por_drivers(expedicao_por_drivers):
            if expedicao_por_drivers < 1:
                return 0
            elif expedicao_por_drivers < 2:
                return 2
            elif expedicao_por_drivers < 3:
                return 6
            elif expedicao_por_drivers < 4:
                return 8
            elif expedicao_por_drivers < 5:
                return 9
            else:
                return 10

        df["score_expedicao_por_drivers"] = df.apply(
            lambda row: get_score_expedicao_por_drivers(row.expedicao_por_drivers),
            axis=1,
        )

        def get_score_percent_criticos(percent):
            if percent < 0.05:
                return 0
            elif percent < 0.08:
                return 5
            elif percent < 0.10:
                return 7
            elif percent < 0.15:
                return 8
            elif percent < 0.20:
                return 9
            else:
                return 10

        df["score_criticos"] = df.apply(
            lambda row: get_score_percent_criticos(row.percent_criticos), axis=1
        )

        def get_score_median_pt(mediana):
            if mediana < 8:
                return 0
            elif mediana < 10:
                return 2
            elif mediana < 15:
                return 5
            elif mediana < 20:
                return 7
            elif mediana < 30:
                return 9
            else:
                return 10

        df["score_pt"] = df.apply(lambda row: get_score_median_pt(row.pt), axis=1)

        def get_score_median_est(mediana):
            if mediana < 3:
                return 0
            elif mediana < 5:
                return 2
            elif mediana < 8:
                return 5
            elif mediana < 10:
                return 8
            elif mediana < 15:
                return 9
            else:
                return 10

        df["score_est"] = df.apply(lambda row: get_score_median_est(row.est), axis=1)

        def get_score_median_tt(mediana):
            if mediana < 8:
                return 0
            elif mediana < 10:
                return 2
            elif mediana < 15:
                return 5
            elif mediana < 20:
                return 8
            else:
                return 10

        df["score_tt"] = df.apply(lambda row: get_score_median_tt(row.tt), axis=1)

        # Calcula Indicador de Saturação

        a = 0.3
        b = 0.2
        c = 0.2
        d = 0.15
        e = 0.08
        f = 0.05
        g = 0.02

        df["indicador_saturacao"] = (
            (df.score_producao_por_drivers * a)
            + (df.score_expedicao_por_drivers * b)
            + (df.score_taxa_ocupacao * c)
            + (df.score_est * d)
            + (df.score_tt * e)
            + (df.score_criticos * f)
            + (df.score_pt * g)
        )

        df.columns = [
            "hub_id",
            "taxa_ocupacao",
            "producao_por_drivers",
            "expedicao_por_drivers",
            "percent_criticos",
            "median_pt",
            "median_est",
            "median_tt",
            "score_taxa_ocupacao",
            "score_producao_por_drivers",
            "score_expedicao_por_drivers",
            "score_criticos",
            "score_pt",
            "score_est",
            "score_tt",
            "indicador_saturacao",
        ]
        return df


def eng_master():
    conn = engine_master()
    eng_master = conn.connect()
    return eng_master


def main():
    global bol_day
    eng_master_conn_apinova = eng_master()
    creation_date = eng_master_conn_apinova.execute(
        "select date(current_timestamp at time zone 'America/Sao_Paulo');"
    ).fetchall()
    eng_master_conn_apinova.close()
    while bol_day == True:
        df_saturacao = saturacao()
        if len(df_saturacao) > 0:
            query_insert_master(
                dataframe=df_saturacao, table_destiny="liveops_saturacao_drivers"
            )
            print("Saturacao - News data :")
        else:
            print("Saturacao -  No news data :")
        eng_master_conn = eng_master()
        same_day = eng_master_conn.execute(
            "select date(current_timestamp at time zone 'America/Sao_Paulo');"
        ).fetchall()
        if creation_date == same_day:
            eng_master_conn.close()
            time.sleep(60)
            bol_day = True
        else:
            print("Encerrando o dia  - Saturacao de Drivers")
            eng_master_conn.close()
            bol_day = False


if __name__ == "__main__":
    main()
