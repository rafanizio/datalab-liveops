def query_orders():
    return """
    select distinct
        oh.order_id,
        date_trunc('second', oh.created_at at  time zone 'America/Sao_Paulo'::text ) as status_created_at,
        date_trunc('second', o.created_at at  time zone 'America/Sao_Paulo'::text ) as order_created_at,
        date_trunc('MILLISECONDS', o.created_at at  time zone 'America/Sao_Paulo'::text ) as order_created_at_ms,
        date_trunc('MILLISECONDS'::text,
                    timezone('America/Sao_Paulo'::text, current_timestamp)) as last_updated,
        UPPER(oh.status) as status_dinamico,
        case when UPPER(oh.status)='CREATED'       then oh.created_at  at  time zone 'America/Sao_Paulo'::text end as created_created_at,
        case when UPPER(oh.status)='ACCEPTED'      then oh.created_at  at  time zone 'America/Sao_Paulo'::text end as accepted_created_at,
        case when UPPER(oh.status)='READY'         then oh.created_at    at  time zone 'America/Sao_Paulo'::text end as ready_created_at,
        case when UPPER(oh.status)='COLLECTED'     then oh.created_at  at  time zone 'America/Sao_Paulo'::text end as collected_created_at,
        case when UPPER(oh.status)='IN_EXPEDITION' then oh.created_at  at  time zone 'America/Sao_Paulo'::text end as in_expedition_created_at,
        case when UPPER(oh.status)='DELIVERING'    then oh.created_at  at  time zone 'America/Sao_Paulo'::text end as delivering_created_at,
        case when UPPER(oh.status)='DELIVERED'     then oh.created_at  at  time zone 'America/Sao_Paulo'::text end as delivered_created_at,
        case when UPPER(oh.status)='FINISHED'      then oh.created_at at  time zone 'America/Sao_Paulo'::text end as finished_created_at,
        op."name" AS hub_name,
        op.id as hub_id,
        s.segment,
        s.id as store_id,
        split_part(s.name::text, '-'::text, 1) AS store_name,
        a.state,
        a.city,
        deliveries.driver_id,
        o.pickup
    FROM order_histories as oh
        JOIN orders o on o.id = oh.order_id
        JOIN stores s ON s.id = o.store_id
        JOIN operation_centers op ON op.id = s.operation_center_id
        LEFT JOIN deliveries ON o.id = deliveries.order_id
        LEFT JOIN addresses a ON a.source_id = op.id AND a.source_type::text = 'OperationCenter'::text
    WHERE date( o.created_at at time zone 'America/Sao_paulo') =
        date( current_date at time zone 'America/Sao_paulo')
    AND upper(op.name::text) !~~* '%TEST%'::text
    AND upper(op.name::text) !~~* '%EQUIPAMENTO%'::text
    AND upper(op.name::text) !~~* '%TREINAMENTO%'::text
    AND  a.deleted_at is NULL
"""


def query_drivers():
    return """
     WITH stations AS (
    SELECT a_1.source_id AS station_id,
           oc.name       AS station_name,
           a_1.latitude,
           a_1.longitude,
           a_1.city,
           a_1.state
    FROM addresses a_1
             JOIN operation_centers oc ON oc.id = a_1.source_id AND a_1.source_type::text = 'OperationCenter'::text
    WHERE a_1.deleted_at IS NULL
      AND (a_1.latitude IS NOT NULL OR a_1.longitude IS NOT NULL)
      AND oc.name::text !~~* '%teste%'::text
      AND oc.name::text !~~* '%treinamento%'::text
),
     driver_opcenter AS (
         SELECT DISTINCT ON (dh_1.driver_id) dh_1.driver_id,
                                             CASE
                                                 WHEN st.station_id IS NULL THEN 0
                                                 ELSE st.station_id
                                                 END AS station_id,
                                             CASE
                                                 WHEN st.station_name IS NULL
                                                     THEN 'Imprecisão da Bringg'::character varying
                                                 ELSE st.station_name
                                                 END AS station_name,
                                             st.city,
                                             st.state
         FROM driver_histories dh_1
                  LEFT JOIN stations st ON st_distancesphere(st_point(dh_1.latitude::double precision,
                                                                      dh_1.longitude::double precision),
                                                             st_point(st.latitude::double precision,
                                                                      st.longitude::double precision)) <=
                                           500::double precision
         WHERE date(timezone('UTC'::text, dh_1.created_at)) = CURRENT_DATE
           AND dh_1.at_home = true
         ORDER BY dh_1.driver_id, dh_1.created_at DESC
     )
SELECT DISTINCT date_trunc('MILLISECONDS'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')  AS creation_ms,
                date_trunc('minute'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')       AS driver_update,
                date_part('hour'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')           AS driver_update_hour,
                date_part('minute'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')         AS driver_update_minute,
                dh.driver_id,
                CASE
                    WHEN dr.driver_type_id = 1 THEN 'Motoboy'::text
                    WHEN dr.driver_type_id = 2 THEN 'Biker'::text
                    ELSE 'Verificar'::text
                    END                                                          AS driver_type,
                lo.independent,
                dh.status,
                dh.at_home,
                CASE
                    WHEN doc.station_id IS NULL THEN 0
                    ELSE doc.station_id
                    END                                                          AS station_id,
                CASE
                    WHEN doc.station_name IS NULL THEN 'Imprecisão da Bringg'::character varying
                    ELSE doc.station_name
                    END                                                          AS station_name,
                CASE
                    WHEN doc.city IS NULL AND a.city::text = ''::text THEN 'Cadastro Incompleto'::character varying
                    ELSE COALESCE(doc.city, a.city)
                    END                                                          AS city,
                CASE
                    WHEN doc.state IS NULL AND a.state::text = ''::text THEN 'Cadastro Incompleto'::character varying
                    ELSE COALESCE(doc.state, a.state)
                    END                                                          AS state
FROM driver_histories dh
         JOIN drivers dr ON dr.id = dh.driver_id
         LEFT JOIN logistics_operators lo ON lo.id = dr.logistics_operator_id
         LEFT JOIN driver_opcenter doc ON doc.driver_id = dh.driver_id
         LEFT JOIN addresses a ON dr.id = a.source_id AND a.source_type::text = 'Driver'::text
WHERE date(timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo') = 
date(timezone('UTC'::text, current_date)at time zone  'America/Sao_paulo') """


def query_drivers_past():
    return """
       WITH stations AS (
    SELECT a_1.source_id AS station_id,
           oc.name       AS station_name,
           a_1.latitude,
           a_1.longitude,
           a_1.city,
           a_1.state
    FROM addresses a_1
             JOIN operation_centers oc ON oc.id = a_1.source_id AND a_1.source_type::text = 'OperationCenter'::text
    WHERE a_1.deleted_at IS NULL
      AND (a_1.latitude IS NOT NULL OR a_1.longitude IS NOT NULL)
      AND oc.name::text !~~* '%teste%'::text
      AND oc.name::text !~~* '%treinamento%'::text
),
     driver_opcenter AS (
         SELECT DISTINCT ON (dh_1.driver_id) dh_1.driver_id,
                                             CASE
                                                 WHEN st.station_id IS NULL THEN 0
                                                 ELSE st.station_id
                                                 END AS station_id,
                                             CASE
                                                 WHEN st.station_name IS NULL
                                                     THEN 'Imprecisão da Bringg'::character varying
                                                 ELSE st.station_name
                                                 END AS station_name,
                                             st.city,
                                             st.state
         FROM driver_histories dh_1
                  LEFT JOIN stations st ON st_distancesphere(st_point(dh_1.latitude::double precision,
                                                                      dh_1.longitude::double precision),
                                                             st_point(st.latitude::double precision,
                                                                      st.longitude::double precision)) <=
                                           500::double precision
WHERE date(timezone('UTC'::text, dh_1.created_at)at time zone  'America/Sao_paulo') >= (CURRENT_DATE - 14)
  AND date(timezone('UTC'::text, dh_1.created_at)at time zone  'America/Sao_paulo') <= (CURRENT_DATE - 1)
           AND dh_1.at_home = true
         ORDER BY dh_1.driver_id, dh_1.created_at DESC
     )
SELECT DISTINCT date_trunc('MILLISECONDS'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')  AS creation_ms,
                date_trunc('minute'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')       AS driver_update,
                date_part('hour'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')           AS driver_update_hour,
                date_part('minute'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')         AS driver_update_minute,
                dh.driver_id,
                CASE
                    WHEN dr.driver_type_id = 1 THEN 'Motoboy'::text
                    WHEN dr.driver_type_id = 2 THEN 'Biker'::text
                    ELSE 'Verificar'::text
                    END                                                          AS driver_type,
                lo.independent,
                dh.status,
                dh.at_home,
                CASE
                    WHEN doc.station_id IS NULL THEN 0
                    ELSE doc.station_id
                    END                                                          AS station_id,
                CASE
                    WHEN doc.station_name IS NULL THEN 'Imprecisão da Bringg'::character varying
                    ELSE doc.station_name
                    END                                                          AS station_name,
                CASE
                    WHEN doc.city IS NULL AND a.city::text = ''::text THEN 'Cadastro Incompleto'::character varying
                    ELSE COALESCE(doc.city, a.city)
                    END                                                          AS city,
                CASE
                    WHEN doc.state IS NULL AND a.state::text = ''::text THEN 'Cadastro Incompleto'::character varying
                    ELSE COALESCE(doc.state, a.state)
                    END                                                          AS state
FROM driver_histories dh
         JOIN drivers dr ON dr.id = dh.driver_id
         LEFT JOIN logistics_operators lo ON lo.id = dr.logistics_operator_id
         LEFT JOIN driver_opcenter doc ON doc.driver_id = dh.driver_id
         LEFT JOIN addresses a ON dr.id = a.source_id AND a.source_type::text = 'Driver'::text
 WHERE date(timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo') >= (CURRENT_DATE - 14)
  AND date(timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo') <= (CURRENT_DATE - 1);
    """


def query_pedidos_drivers():
    return """
            with drivers as (
            SELECT
                hub_id,
                count(distinct driver_id) filter(where at_home = TRUE) as drivers_at_home,
                count(distinct driver_id) filter(where status = 'online') as drivers_on_shift
            FROM (
                    SELECT DISTINCT ON (driver_id)
                    station_id as hub_id,
                    driver_id,
                    status,
                    at_home
                    FROM public.mvw_liveops_drivers
                    ORDER BY driver_id, driver_update desc
                ) as drivers_realtime
            group by hub_id
    ),
    orders as (
            select
                hub_id,
                sum(pedido_aceito + pedido_pronto) as pedidos_producao,
                sum(pedido_collected + pedido_in_expedition) as pedidos_expedicao
            from public.mvw_liveops_orders
            where segment = 'FOODS'
            and qtd_pedidos > 0
            group by hub_id
    )
    select
        orders.hub_id,
        orders.pedidos_producao,
        orders.pedidos_expedicao,
        coalesce(drivers.drivers_at_home, 0) as at_home,
        coalesce(drivers.drivers_on_shift, 0) as on_shift
    from orders
    left join drivers on drivers.hub_id = orders.hub_id;

    """


def query_sla():
    return """
    select
        hub_id,
        order_id,
        pt_medio as pt,
        est_medio as est,
        tt_medio as tt
    from public.mvw_liveops_orders
    where order_created  between (timezone('America/Sao_Paulo', current_timestamp) - interval '2 hours') and (timezone('America/Sao_Paulo', current_timestamp) - interval '1 hours')
        and segment = 'FOODS'
        and (pedido_entregue > 0 or pedido_finalizado > 0)
        and (pt_medio > 0 or est_medio > 0 or tt_medio > 0)
        order by hub_id asc;
    """