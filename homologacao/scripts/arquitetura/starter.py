from dao import *
from drop_table import *
from create_tb import *
from create_mvw import *
from querys import *
from refresh_mvw import *
import time
from sqlalchemy import create_engine


def eng_master():
    conn = engine_master()
    eng_master = conn.connect()
    return eng_master


def drop_tb(eng_master):
    eng_master.execute(drop_orders())
    print("Drop orders feito!")
    time.sleep(1)
    eng_master.execute(drop_saturacao_drivers())
    print("Drop drop_saturacao_drivers  feito!")
    time.sleep(1)
    eng_master.execute(drop_drivers())
    print("Drop drivers feito!")
    time.sleep(1)
    eng_master.execute(drop_apinova())
    print("Drop api_nova  feito!")
    time.sleep(1)
    eng_master.execute(drop_tuktuk())
    time.sleep(1)
    print("Drop tuktuk feito!")
    eng_master.execute(drop_label())
    print("Drop Language feito!")
    eng_master.close()


def create_tb(eng_master):
    eng_master.execute(create_orders())
    print("create tb_orders feito!")
    time.sleep(1)
    eng_master.execute(create_drivers())
    print("create tb_drivers feito!")
    time.sleep(1)
    eng_master.execute(create_apinova())
    print("create tb_api_nova feito!")
    time.sleep(1)
    eng_master.execute(create_saturacao())
    print("create  tb_saturacao_drivers feito!")
    time.sleep(1)
    eng_master.execute(create_tuktuk())
    print("create  tb_tuktuk feito!")
    time.sleep(1)
    eng_master.execute(create_label_language())
    time.sleep(1)
    print("create tb_language feito!")
    eng_master.close()


def create_mvw(eng_master):
    eng_master.execute(create_mvw_orders())
    print("create mvw_orders feito!")
    time.sleep(1)
    eng_master.execute(create_mvw_orders_past())
    print("create mvw_orders_past feito!")
    time.sleep(1)
    eng_master.execute(create_mvw_drivers())
    print("create mvw_drivers!")
    time.sleep(1)
    eng_master.execute(create_mvw_drivers_past())
    print("create mvw_drivers_past!")
    time.sleep(1)
    eng_master.execute(create_mvw_status_histories())
    time.sleep(1)
    print("create mvw_status_histories feito!")
    eng_master.execute(create_mvw_status_histories_past())
    print("create mvw_status_histories_past feito!")
    time.sleep(1)
    eng_master.execute(create_mvw_monitor_frete())
    print("create mvw_status_histories_past feito!")
    time.sleep(1)
    eng_master.execute(create_label_language())
    print("create mvw_status_histories_past feito!")
    time.sleep(1)
    eng_master.close()


def create_idx(eng_master):
    eng_master.execute(create_idx_orders())
    print("create idx_orders feito!")
    time.sleep(1)
    eng_master.execute(create_idx_orders_past())
    print("create idx_orders_past feito!")
    time.sleep(1)
    eng_master.execute(create_idx_drivers())
    print("create idx_drivers!")
    time.sleep(1)
    eng_master.execute(create_idx_drivers_past())
    print("create idx_drivers_past!")
    time.sleep(1)
    eng_master.execute(create_idx_status_histories())
    time.sleep(1)
    print("create idx_status_histories feito!")
    eng_master.execute(create_idx_status_histories_past())
    print("create idx_status_histories_past feito!")
    time.sleep(1)
    eng_master.execute(create_idx_monitor())
    print("create idx monitor feito!")
    time.sleep(1)
    eng_master.close()


def orders(eng_master):
    eng_master.execute("set timezone = 'America/Sao_Paulo'")
    df_orders = query_get_replica(query_orders())
    query_insert_master(dataframe=df_orders, table_destiny="liveops_orders")
    print("orders feito!")
    eng_master.close()


def orders_past(eng_master):
    eng_master.execute("set timezone = 'America/Sao_Paulo'")

    df_orders_past = query_get_replica(query_orders_past())
    query_insert_master(dataframe=df_orders_past, table_destiny="liveops_orders_past")
    print("orders_past feito!")
    eng_master.close()


def drivers(eng_master):
    eng_master.execute("set timezone = 'America/Sao_Paulo'")

    df_drivers = query_get_replica(query_drivers())
    query_insert_master(dataframe=df_drivers, table_destiny="liveops_drivers")
    print("drivers feito!")
    eng_master.close()


def drivers_past(eng_master):
    eng_master.execute("set timezone = 'America/Sao_Paulo'")

    df_drivers_past = query_get_replica(query_drivers_past())
    query_insert_master(dataframe=df_drivers_past, table_destiny="liveops_drivers_past")
    print("drivers_past feito!")
    eng_master.close()


def status_histories(eng_master):
    eng_master.execute("set timezone = 'America/Sao_Paulo'")
    df_status_histories = query_get_replica(query_status_histories())
    query_insert_master(
        dataframe=df_status_histories, table_destiny="liveops_status_histories"
    )
    eng_master.close()


def status_histories_past(eng_master):
    eng_master.execute("set timezone = 'America/Sao_Paulo'")
    df_status_histories = query_get_replica(query_status_histories())
    print("status_histories feito!")
    df_status_histories_past = query_get_replica(query_status_histories_past())
    query_insert_master(
        dataframe=df_status_histories_past,
        table_destiny="liveops_status_histories_past",
    )
    eng_master.close()


def refresh_mvw(eng_master):
    print("---------------Inicializando REFRESH DA ORDERS---------------")
    eng_master.execute(refresh_mvw_orders())
    eng_master.execute(refresh_mvw_orders_past())
    print("---------------Inicializando REFRESH DA Drivers---------------")
    eng_master.execute(refresh_mvw_drivers())
    eng_master.execute(refresh_mvw_drivers_past())
    print("---------------Inicializando REFRESH DA Status histories---------------")
    eng_master.execute(refresh_mvw_status_histories())
    eng_master.execute(refresh_mvw_status_histories_past())
    eng_master.close()


def run():
    print("---------------DROP TABELAS CASCADE---------------")
    drop_tb(eng_master())
    print("---------------CREATE TABELAS---------------")
    create_tb(eng_master())
    print("---------------CREATE MVW---------------")
    create_mvw(eng_master())
    print("---------------CREATE IDX---------------")
    create_idx(eng_master())
    print("---------------Inicializando as primeiras querys do dia---------------")
    orders(eng_master())
    orders_past(eng_master())
    drivers(eng_master())
    drivers_past(eng_master())
    status_histories(eng_master())
    status_histories_past(eng_master())
    print("---------------Inicializando  REFRESH das MVW---------------")
    refresh_mvw(eng_master())
    print("---------------BANCO INICIALIZADO COM SUCESSO---------------")


if __name__ == "__main__":

    run()
