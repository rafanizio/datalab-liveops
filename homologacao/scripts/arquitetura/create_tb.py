def create_label_language():

    return """ create table if not exists label_language
    (
        id serial not null,
        label_languages varchar(50) not null,
        global_title_orders varchar(50) not null,
        global_title_foods_kpi varchar(50) not null,
        global_title_drivers varchar(50) not null,
        global_title_sellers varchar(50) not null,
        global_box_total_orders varchar(50) not null,
        global_box_total_orders_d7 varchar(50) not null,
        global_box_oct varchar(50) not null,
        global_box_critical_orders_oct varchar(50) not null,
        global_box_ttest varchar(50) not null,
        global_box_critical_orders_tt_est varchar(50) not null,
        global_box_active_drivers varchar(50) not null,
        global_box_active_drivers_d7 varchar(50) not null,
        global_box_drivers_onshift varchar(50) not null,
        global_box_drivers_occupation_rate varchar(50) not null,
        global_box_active_sellers varchar(50) not null,
        global_box_active_sellers_d7 varchar(50) not null,
        global_table_stations varchar(50) not null,
        global_graph1_accum varchar(50) not null,
        global_graph2_allstatus varchar(50) not null,
        global_graph3_statuscreated varchar(50) not null,
        global_graph4_statusready varchar(50) not null,
        global_graph5_statusready varchar(50) not null,
        global_box_latest_update varchar(50)
    );"""


def create_drivers():

    return """ create table if not exists liveops_drivers
    (
        index serial not null,
        driver_creation_ms timestamp,
        driver_update timestamp,
        driver_update_hour double precision,
        driver_update_minute double precision,
        driver_id bigint,
        driver_type text,
        independent boolean,
        status text,
        at_home boolean,
        station_id bigint,
        station_name text,
        city text,
        state text,
        last_updated timestamp default CURRENT_TIMESTAMP
    ); """


def create_orders():
    return """
        create table if not exists liveops_orders
    (
        order_id integer,
        status_created_at timestamp,
        order_created_at timestamp,
        order_created_at_ms timestamp,
        last_updated timestamp,
        status_dinamico varchar(20),
        created_created_at timestamp,
        accepted_created_at timestamp,
        ready_created_at timestamp,
        collected_created_at timestamp,
        in_expedition_created_at timestamp,
        delivering_created_at timestamp,
        delivered_created_at timestamp,
        finished_created_at timestamp,
        hub_name varchar(200),
        hub_id integer,
        segment varchar(20),
        store_id integer,
        store_name varchar(200),
        state varchar(20),
        city varchar(200),
        driver_id integer,
        pickup boolean
    );

    """


def create_tuktuk():
    return """ 
    create table if not exists liveops_tuktuk
    (
        pedidos varchar,
        races_id bigserial not null,
        ordem_casamento bigint,
        hub_name text,
        route_distance_in_meters integer,
        freight_races numeric,
        inserted_at timestamp,
        insert_formart_ms timestamp with time zone,
        link_rota_completa text
    );"""


def create_apinova():
    return """create table if not exists liveops_api_nova
    (
        hub_orders_id varchar,
        store_name varchar,
        segmento varchar,
        banda_extra bigint,
        freight_deliveries_price numeric,
        motivo_banda_extra varchar,
        tipo_banda_extra varchar,
        deliveries_createad_ms timestamp
    ); """


def create_saturacao():
    return """create table if not exists liveops_saturacao_drivers
    (
        inserted_at timestamp default timezone('America/Sao_Paulo'::text, now()),
        hub_id bigint,
        taxa_ocupacao double precision,
        producao_por_drivers double precision,
        percent_criticos double precision,
        expedicao_por_drivers double precision,
        median_pt bigint,
        median_est bigint,
        median_tt bigint,
        score_taxa_ocupacao bigint,
        score_producao_por_drivers bigint,
        score_expedicao_por_drivers bigint,
        score_criticos bigint,
        score_pt bigint,
        score_est bigint,
        score_tt bigint,
        indicador_saturacao double precision
    );"""