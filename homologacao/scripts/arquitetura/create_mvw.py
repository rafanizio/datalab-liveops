def create_mvw_orders():
    return """

    create materialized view if not exists mvw_liveops_orders as
        WITH oct AS (
        SELECT liveops_orders.order_id,
            max(liveops_orders.created_created_at)       AS max_created_created_at,
            max(liveops_orders.accepted_created_at)      AS max_accepted_created_at,
            max(liveops_orders.ready_created_at)         AS max_ready_created_at,
            max(liveops_orders.collected_created_at)     AS max_collected_created_at,
            max(liveops_orders.in_expedition_created_at) AS max_in_expedition_created_at,
            max(liveops_orders.delivering_created_at)    AS max_delivering_created_at,
            max(liveops_orders.delivered_created_at)     AS max_delivered_created_at,
            max(liveops_orders.finished_created_at)      AS max_finished_created_at
        FROM liveops_orders
        GROUP BY liveops_orders.order_id
    )
    SELECT DISTINCT ON (liveops.order_id) CASE
                                            WHEN liveops.status_dinamico::text <> ALL
                                                (ARRAY ['REFUSED'::text, 'CANCELED'::text])
                                                THEN count(DISTINCT liveops.order_id)
                                            ELSE 0::bigint
                                            END                                                                                                                             AS qtd_pedidos,
                                        COALESCE(avg(date_part('minute'::text,
                                                                COALESCE(oct.max_delivered_created_at, oct.max_finished_created_at) -
                                                                COALESCE(oct.max_accepted_created_at, oct.max_created_created_at)) +
                                                    date_part('hour'::text,
                                                                COALESCE(oct.max_delivered_created_at, oct.max_finished_created_at) -
                                                                COALESCE(oct.max_accepted_created_at, oct.max_created_created_at)) *
                                                    60::double precision + date_part('day'::text,
                                                                                        COALESCE(oct.max_delivered_created_at, oct.max_finished_created_at) -
                                                                                        COALESCE(oct.max_accepted_created_at, oct.max_created_created_at)) *
                                                                            60::double precision * 24::double precision)
                                                FILTER (WHERE liveops.pickup = false),
                                                0::double precision)                                                                                                       AS oct_medio,
                                        COALESCE(avg(date_part('minute'::text, oct.max_ready_created_at -
                                                                                COALESCE(oct.max_accepted_created_at, oct.max_created_created_at)) +
                                                    date_part('hour'::text, oct.max_ready_created_at -
                                                                            COALESCE(oct.max_accepted_created_at, oct.max_created_created_at)) *
                                                    60::double precision +
                                                    date_part('day'::text, oct.max_ready_created_at -
                                                                            COALESCE(oct.max_accepted_created_at, oct.max_created_created_at)) *
                                                    60::double precision * 24::double precision)
                                                FILTER (WHERE liveops.pickup = false),
                                                0::double precision)                                                                                                       AS pt_medio,
                                        COALESCE(avg(date_part('minute'::text,
                                                                oct.max_delivering_created_at - oct.max_ready_created_at) +
                                                    date_part('hour'::text,
                                                                oct.max_delivering_created_at - oct.max_ready_created_at) *
                                                    60::double precision + date_part('day'::text,
                                                                                        oct.max_delivering_created_at - oct.max_ready_created_at) *
                                                                            60::double precision * 24::double precision)
                                                FILTER (WHERE liveops.pickup = false),
                                                0::double precision)                                                                                                       AS est_medio,
                                        COALESCE(avg(date_part('minute'::text,
                                                                COALESCE(oct.max_delivered_created_at, oct.max_finished_created_at) -
                                                                oct.max_delivering_created_at) + date_part('hour'::text,
                                                                                                            COALESCE(oct.max_delivered_created_at, oct.max_finished_created_at) -
                                                                                                            oct.max_delivering_created_at) *
                                                                                                60::double precision +
                                                    date_part('day'::text,
                                                                COALESCE(oct.max_delivered_created_at, oct.max_finished_created_at) -
                                                                oct.max_delivering_created_at) * 60::double precision *
                                                    24::double precision) FILTER (WHERE liveops.pickup = false),
                                                0::double precision)                                                                                                       AS tt_medio,
                                        liveops.hub_name,
                                        liveops.hub_id,
                                        liveops.order_created_at                                                                                                            AS order_created,
                                        date_part('hour'::text, liveops.order_created_at)                                                                                   AS created_at_hour,
                                        date_part('minute'::text, liveops.order_created_at)                                                                                 AS created_at_min,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'CREATED'::text)                                                                      AS pedido_criado,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'ACCEPTED'::text)                                                                     AS pedido_aceito,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'READY'::text)                                                                        AS pedido_pronto,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'COLLECTED'::text)                                                                    AS pedido_collected,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'IN_EXPEDITION'::text)                                                                AS pedido_in_expedition,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'DELIVERING'::text)                                                                   AS pedido_entregando,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'DELIVERED'::text)                                                                    AS pedido_entregue,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'FINISHED'::text)                                                                     AS pedido_finalizado,
                                        count(DISTINCT liveops.order_id) FILTER (WHERE liveops.status_dinamico::text = ANY
                                                                                        (ARRAY ['ACCEPTED'::text, 'READY'::text, 'COLLECTED'::text, 'IN_EXPEDITION'::text])) AS pedidos_planejamento,
                                        CASE
                                            WHEN (liveops.store_id = ANY
                                                    (ARRAY [524, 1019, 3019, 3073, 763, 1020, 3061, 1538, 1083, 3044, 2861, 3062, 3017, 1355, 2888, 2925, 2933, 2902, 1084, 1539, 3012, 1070, 1079, 1122, 2051, 2703, 1145, 1004, 1121, 1118, 755, 88, 693, 2012, 2014, 2013, 1784, 1571, 427, 989, 921, 1734, 953, 932, 2645, 960, 603, 915, 2762, 1962, 2103, 640, 600, 965, 1204, 1034, 1062, 923, 1064, 2761, 964, 1686, 3433, 3257, 968, 967, 1044, 1003, 3124, 2554, 1128, 595, 3172, 2007, 2664, 2163, 633, 1041, 893, 3024, 1108, 1007, 984, 1120, 754, 1030, 739, 993, 1221, 2785, 618, 2604, 482, 480, 1099, 1091, 1093, 1092, 1101, 1102, 1098, 1103, 1104, 1105, 1094, 1096, 1100, 1097, 1106, 1095, 2498, 2493, 2499, 2494, 2492, 2517, 2497, 1071])) AND
                                                liveops.segment::text = 'FOODS'::text THEN 'GOODS'::character varying
                                            ELSE liveops.segment
                                            END                                                                                                                             AS segment,
                                        liveops.store_id,
                                        liveops.store_name,
                                        liveops.state,
                                        liveops.city,
                                        liveops.driver_id,
                                        liveops.order_id
    FROM liveops_orders liveops
            JOIN oct ON oct.order_id = liveops.order_id
    WHERE date(liveops.order_created_at) = date(timezone('America/Sao_Paulo'::text, CURRENT_TIMESTAMP))
    GROUP BY liveops.status_dinamico, liveops.hub_name, liveops.hub_id, liveops.order_created_at, liveops.status_created_at,
            (date_part('hour'::text, liveops.order_created_at)), (date_part('minute'::text, liveops.order_created_at)),
            liveops.segment, liveops.store_id, liveops.store_name, liveops.state, liveops.city, liveops.driver_id,
            liveops.order_id
    ORDER BY liveops.order_id, liveops.status_created_at DESC;
 """


def create_mvw_orders_past():
    return """
            create materialized view if not exists mvw_liveops_orders_past as
        WITH oct AS (
        SELECT liveops_orders.order_id,
            max(liveops_orders.created_created_at)       AS max_created_created_at,
            max(liveops_orders.accepted_created_at)      AS max_accepted_created_at,
            max(liveops_orders.ready_created_at)         AS max_ready_created_at,
            max(liveops_orders.collected_created_at)     AS max_collected_created_at,
            max(liveops_orders.in_expedition_created_at) AS max_in_expedition_created_at,
            max(liveops_orders.delivering_created_at)    AS max_delivering_created_at,
            max(liveops_orders.delivered_created_at)     AS max_delivered_created_at,
            max(liveops_orders.finished_created_at)      AS max_finished_created_at
        FROM liveops_orders
        GROUP BY liveops_orders.order_id
    )
    SELECT DISTINCT ON (liveops.order_id) CASE
                                            WHEN liveops.status_dinamico::text <> ALL
                                                (ARRAY ['REFUSED'::text, 'CANCELED'::text])
                                                THEN count(DISTINCT liveops.order_id)
                                            ELSE 0::bigint
                                            END                                                                                                                             AS qtd_pedidos,
                                        COALESCE(avg(date_part('minute'::text,
                                                                COALESCE(oct.max_delivered_created_at, oct.max_finished_created_at) -
                                                                COALESCE(oct.max_accepted_created_at, oct.max_created_created_at)) +
                                                    date_part('hour'::text,
                                                                COALESCE(oct.max_delivered_created_at, oct.max_finished_created_at) -
                                                                COALESCE(oct.max_accepted_created_at, oct.max_created_created_at)) *
                                                    60::double precision + date_part('day'::text,
                                                                                        COALESCE(oct.max_delivered_created_at, oct.max_finished_created_at) -
                                                                                        COALESCE(oct.max_accepted_created_at, oct.max_created_created_at)) *
                                                                            60::double precision * 24::double precision)
                                                FILTER (WHERE liveops.pickup = false),
                                                0::double precision)                                                                                                       AS oct_medio,
                                        COALESCE(avg(date_part('minute'::text, oct.max_ready_created_at -
                                                                                COALESCE(oct.max_accepted_created_at, oct.max_created_created_at)) +
                                                    date_part('hour'::text, oct.max_ready_created_at -
                                                                            COALESCE(oct.max_accepted_created_at, oct.max_created_created_at)) *
                                                    60::double precision +
                                                    date_part('day'::text, oct.max_ready_created_at -
                                                                            COALESCE(oct.max_accepted_created_at, oct.max_created_created_at)) *
                                                    60::double precision * 24::double precision)
                                                FILTER (WHERE liveops.pickup = false),
                                                0::double precision)                                                                                                       AS pt_medio,
                                        COALESCE(avg(date_part('minute'::text,
                                                                oct.max_delivering_created_at - oct.max_ready_created_at) +
                                                    date_part('hour'::text,
                                                                oct.max_delivering_created_at - oct.max_ready_created_at) *
                                                    60::double precision + date_part('day'::text,
                                                                                        oct.max_delivering_created_at - oct.max_ready_created_at) *
                                                                            60::double precision * 24::double precision)
                                                FILTER (WHERE liveops.pickup = false),
                                                0::double precision)                                                                                                       AS est_medio,
                                        COALESCE(avg(date_part('minute'::text,
                                                                COALESCE(oct.max_delivered_created_at, oct.max_finished_created_at) -
                                                                oct.max_delivering_created_at) + date_part('hour'::text,
                                                                                                            COALESCE(oct.max_delivered_created_at, oct.max_finished_created_at) -
                                                                                                            oct.max_delivering_created_at) *
                                                                                                60::double precision +
                                                    date_part('day'::text,
                                                                COALESCE(oct.max_delivered_created_at, oct.max_finished_created_at) -
                                                                oct.max_delivering_created_at) * 60::double precision *
                                                    24::double precision) FILTER (WHERE liveops.pickup = false),
                                                0::double precision)                                                                                                       AS tt_medio,
                                        liveops.hub_name,
                                        liveops.hub_id,
                                        liveops.order_created_at                                                                                                            AS order_created,
                                        date_part('hour'::text, liveops.order_created_at)                                                                                   AS created_at_hour,
                                        date_part('minute'::text, liveops.order_created_at)                                                                                 AS created_at_min,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'CREATED'::text)                                                                      AS pedido_criado,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'ACCEPTED'::text)                                                                     AS pedido_aceito,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'READY'::text)                                                                        AS pedido_pronto,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'COLLECTED'::text)                                                                    AS pedido_collected,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'IN_EXPEDITION'::text)                                                                AS pedido_in_expedition,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'DELIVERING'::text)                                                                   AS pedido_entregando,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'DELIVERED'::text)                                                                    AS pedido_entregue,
                                        count(DISTINCT liveops.order_id)
                                        FILTER (WHERE liveops.status_dinamico::text = 'FINISHED'::text)                                                                     AS pedido_finalizado,
                                        count(DISTINCT liveops.order_id) FILTER (WHERE liveops.status_dinamico::text = ANY
                                                                                        (ARRAY ['ACCEPTED'::text, 'READY'::text, 'COLLECTED'::text, 'IN_EXPEDITION'::text])) AS pedidos_planejamento,
                                        CASE
                                            WHEN (liveops.store_id = ANY
                                                    (ARRAY [524, 1019, 3019, 3073, 763, 1020, 3061, 1538, 1083, 3044, 2861, 3062, 3017, 1355, 2888, 2925, 2933, 2902, 1084, 1539, 3012, 1070, 1079, 1122, 2051, 2703, 1145, 1004, 1121, 1118, 755, 88, 693, 2012, 2014, 2013, 1784, 1571, 427, 989, 921, 1734, 953, 932, 2645, 960, 603, 915, 2762, 1962, 2103, 640, 600, 965, 1204, 1034, 1062, 923, 1064, 2761, 964, 1686, 3433, 3257, 968, 967, 1044, 1003, 3124, 2554, 1128, 595, 3172, 2007, 2664, 2163, 633, 1041, 893, 3024, 1108, 1007, 984, 1120, 754, 1030, 739, 993, 1221, 2785, 618, 2604, 482, 480, 1099, 1091, 1093, 1092, 1101, 1102, 1098, 1103, 1104, 1105, 1094, 1096, 1100, 1097, 1106, 1095, 2498, 2493, 2499, 2494, 2492, 2517, 2497, 1071])) AND
                                                liveops.segment::text = 'FOODS'::text THEN 'GOODS'::character varying
                                            ELSE liveops.segment
                                            END                                                                                                                             AS segment,
                                        liveops.store_id,
                                        liveops.store_name,
                                        liveops.state,
                                        liveops.city,
                                        liveops.driver_id,
                                        liveops.order_id
    FROM liveops_orders liveops
            JOIN oct ON oct.order_id = liveops.order_id
    WHERE date(liveops.order_created_at) >=
        (date(timezone('America/Sao_Paulo'::text, CURRENT_TIMESTAMP)) - '14 days'::interval)
    AND date(liveops.order_created_at) <=
        (date(timezone('America/Sao_Paulo'::text, CURRENT_TIMESTAMP)) - '1 day'::interval)
    GROUP BY liveops.status_dinamico, liveops.hub_name, liveops.hub_id, liveops.order_created_at, liveops.status_created_at,
            (date_part('hour'::text, liveops.order_created_at)), (date_part('minute'::text, liveops.order_created_at)),
            liveops.segment, liveops.store_id, liveops.store_name, liveops.state, liveops.city, liveops.driver_id,
            liveops.order_id
    ORDER BY liveops.order_id, liveops.status_created_at DESC;
       
    """


def create_mvw_drivers():
    return """
        create materialized view if not exists mvw_liveops_drivers as
	SELECT liveops_drivers.index,
       liveops_drivers.driver_creation_ms,
       liveops_drivers.driver_update,
       liveops_drivers.driver_update_hour,
       liveops_drivers.driver_update_minute,
       liveops_drivers.driver_id,
       liveops_drivers.driver_type,
       liveops_drivers.independent,
       liveops_drivers.status,
       liveops_drivers.at_home,
       liveops_drivers.station_id,
       liveops_drivers.station_name,
       liveops_drivers.city,
       liveops_drivers.state,
       liveops_drivers.last_updated
    FROM liveops_drivers
    WHERE date(liveops_drivers.driver_update) = date(timezone('America/Sao_Paulo'::text, CURRENT_TIMESTAMP));
        """


def create_mvw_drivers_past():
    return """
            create materialized view if not exists mvw_liveops_drivers_past as
        SELECT liveops_drivers.index,
        liveops_drivers.driver_creation_ms,
        liveops_drivers.driver_update,
        liveops_drivers.driver_update_hour,
        liveops_drivers.driver_update_minute,
        liveops_drivers.driver_id,
        liveops_drivers.driver_type,
        liveops_drivers.independent,
        liveops_drivers.status,
        liveops_drivers.at_home,
        liveops_drivers.station_id,
        liveops_drivers.station_name,
        liveops_drivers.city,
        liveops_drivers.state,
        liveops_drivers.last_updated
    FROM liveops_drivers
    WHERE date(liveops_drivers.driver_update) >=
        (date(timezone('America/Sao_Paulo'::text, CURRENT_TIMESTAMP)) - '14 days'::interval)
    AND date(liveops_drivers.driver_update) <=
        (date(timezone('America/Sao_Paulo'::text, CURRENT_TIMESTAMP)) - '1 day'::interval);
    """


def create_mvw_status_histories():
    return """
        create materialized view if not exists mvw_liveops_status_histories as
        SELECT DISTINCT liveops_orders.hub_name,
                    liveops_orders.hub_id,
                    date_trunc('MILLISECONDS'::text, liveops_orders.status_created_at)          AS status_created,
                    date_part('hour'::text, liveops_orders.status_created_at)                   AS created_at_hour,
                    date_part('minute'::text, liveops_orders.status_created_at)                 AS created_at_min,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'CREATED'::text)       AS pedido_criado,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'ACCEPTED'::text)      AS pedido_aceito,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'READY'::text)         AS pedido_pronto,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'COLLECTED'::text)     AS pedido_collected,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'IN_EXPEDITION'::text) AS pedido_in_expedition,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'DELIVERING'::text)    AS pedido_entregando,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'DELIVERED'::text)     AS pedido_entregue,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'FINISHED'::text)      AS pedido_finalizado,
                    liveops_orders.segment,
                    liveops_orders.store_id,
                    liveops_orders.store_name,
                    liveops_orders.state,
                    liveops_orders.city
    FROM liveops_orders
    WHERE date(liveops_orders.order_created_at) = date(timezone('America/Sao_Paulo'::text, CURRENT_TIMESTAMP))
    GROUP BY liveops_orders.hub_name, liveops_orders.hub_id,
            (date_trunc('MILLISECONDS'::text, liveops_orders.status_created_at)),
            (date_part('hour'::text, liveops_orders.status_created_at)),
            (date_part('minute'::text, liveops_orders.status_created_at)), liveops_orders.segment, liveops_orders.store_id,
            liveops_orders.store_name, liveops_orders.state, liveops_orders.city
    ORDER BY (date_trunc('MILLISECONDS'::text, liveops_orders.status_created_at));
    """


def create_mvw_status_past():
    return """"
        create materialized view if not exists mvw_liveops_status_histories_past as
        SELECT DISTINCT liveops_orders.hub_name,
                    liveops_orders.hub_id,
                    date_trunc('MILLISECONDS'::text, liveops_orders.status_created_at)          AS status_created,
                    date_part('hour'::text, liveops_orders.status_created_at)                   AS created_at_hour,
                    date_part('minute'::text, liveops_orders.status_created_at)                 AS created_at_min,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'CREATED'::text)       AS pedido_criado,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'ACCEPTED'::text)      AS pedido_aceito,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'READY'::text)         AS pedido_pronto,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'COLLECTED'::text)     AS pedido_collected,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'IN_EXPEDITION'::text) AS pedido_in_expedition,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'DELIVERING'::text)    AS pedido_entregando,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'DELIVERED'::text)     AS pedido_entregue,
                    count(DISTINCT liveops_orders.order_id)
                    FILTER (WHERE liveops_orders.status_dinamico::text = 'FINISHED'::text)      AS pedido_finalizado,
                    liveops_orders.segment,
                    liveops_orders.store_id,
                    liveops_orders.store_name,
                    liveops_orders.state,
                    liveops_orders.city
    FROM liveops_orders
    WHERE date(liveops_orders.order_created_at) >=
        (date(timezone('America/Sao_Paulo'::text, CURRENT_TIMESTAMP)) - '14 days'::interval)
    AND date(liveops_orders.order_created_at) <=
        (date(timezone('America/Sao_Paulo'::text, CURRENT_TIMESTAMP)) - '1 day'::interval)
    GROUP BY liveops_orders.hub_name, liveops_orders.hub_id,
            (date_trunc('MILLISECONDS'::text, liveops_orders.status_created_at)),
            (date_part('hour'::text, liveops_orders.status_created_at)),
            (date_part('minute'::text, liveops_orders.status_created_at)), liveops_orders.segment, liveops_orders.store_id,
            liveops_orders.store_name, liveops_orders.state, liveops_orders.city
    ORDER BY (date_trunc('MILLISECONDS'::text, liveops_orders.status_created_at));
    """


def create_mvw_monitor_frete():
    return """
        create materialized view mvw_liveops_monitor_frete as
        SELECT hub.hub_orders_id,
            rof.hub_name,
            hub.store_name,
            hub.segmento,
            hub.banda_extra,
            hub.motivo_banda_extra,
            hub.tipo_banda_extra,
            hub.freight_deliveries_price,
            rof.freight_races,
            rof.races_id,
            rof.route_distance_in_meters,
            rof.inserted_at,
            rof.link_rota_completa,
            rof.insert_formart_ms
        FROM api_nova hub
                JOIN liveops_tuktuk rof ON rof.pedidos::text = hub.hub_orders_id::text
        where date(inserted_at) = date(timezone('America/Sao_Paulo'::text, CURRENT_TIMESTAMP))
    """


def create_idx_orders():
    return """
        create unique index if not exists idx_liveops_orders
	on mvw_liveops_orders (qtd_pedidos, oct_medio, pt_medio, est_medio, tt_medio, hub_name, hub_id, order_created, 
	                       created_at_hour, created_at_min, pedido_criado, pedido_aceito, pedido_pronto, 
	                       pedido_collected, pedido_in_expedition, pedido_entregando, pedido_entregue,
	                       pedido_finalizado, pedidos_planejamento, segment, store_id, store_name, 
	                       state, city, driver_id, order_id);
    """


def create_idx_orders_past():
    return """
    create unique index if not exists idx_liveops_orders_past
	on mvw_liveops_orders_past (qtd_pedidos, oct_medio, pt_medio, est_medio, tt_medio, hub_name, 
    hub_id, order_created, created_at_hour, created_at_min, pedido_criado, pedido_aceito, pedido_pronto, 
    pedido_collected, pedido_in_expedition, pedido_entregando, pedido_entregue, pedido_finalizado,
     pedidos_planejamento, segment, store_id, store_name, state, city, driver_id, order_id); """


def create_idx_drivers():
    return """
        create unique index if not exists idx_drivers
	on mvw_liveops_drivers (index, driver_update, driver_update_hour, driver_update_minute,
	    driver_id, driver_type, independent, status, at_home, station_id,
	    station_name, city, state, last_updated);
     """


def create_idx_drivers_past():
    return """
    create unique index if not exists idx_drivers_past
	on mvw_liveops_drivers_past (index, driver_update, driver_update_hour, 
	                             driver_update_minute, driver_id, driver_type, independent, status, 
	                             at_home, station_id, station_name, city, state, last_updated);
    """


def create_idx_status():
    return """
        create unique index if not exists idx_status
	on mvw_liveops_status_histories (hub_name, hub_id, status_created, created_at_hour, created_at_min, 
	                                 pedido_criado, pedido_aceito, pedido_pronto, pedido_collected, 
	                                 pedido_in_expedition, pedido_entregando, pedido_entregue, 
	                                 pedido_finalizado, segment, store_id, store_name, state, city);
    """


def create_idx_status_past():
    """
    create unique index if not exists idx_status_past
        on mvw_liveops_status_histories_past (hub_name, hub_id, status_created, created_at_hour, created_at_min, pedido_criado, pedido_aceito,
                                            pedido_pronto, pedido_collected, pedido_in_expedition,
                                            pedido_entregando, pedido_entregue, pedido_finalizado, segment,
                                            store_id, store_name, state, city);"""


def create_idx_monitor():
    return """
        create unique index if not exists idx_liveops_monitor__frete
	on mvw_liveops_monitor_frete (hub_orders_id, hub_name, store_name, segmento, banda_extra, motivo_banda_extra, 
    tipo_banda_extra, freight_deliveries_price, freight_races, races_id, route_distance_in_meters, inserted_at, 
    link_rota_completa, insert_formart_ms);
         """
