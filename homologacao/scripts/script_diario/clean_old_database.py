from dao import *
from querys_incremen import query_orders_inc
import time
from sqlalchemy import create_engine

bol_day = True


def eng_master():
    conn = engine_master()
    eng_master = conn.connect()
    return eng_master


def run_delete_old_orders():
    eng_master_conn = eng_master()
    eng_master_conn.execute(
        "delete from liveops_orders where date(order_created_at) <  date(current_timestamp - interval '15 days');"
    )
    print("Delete orders < D15")
    eng_master_conn.close()


def run_delete_old_drivers():
    eng_master_conn = eng_master()
    eng_master_conn.execute(
        "delete from liveops_drivers where date(driver_update) <  date(current_timestamp - interval '15 days');"
    )
    print("Delete drivers past < D15")
    eng_master_conn.close()


def run_delete_old_saturacao():
    eng_master_conn = eng_master()
    eng_master_conn.execute(
        "delete from public.liveops_saturacao_drivers where date(inserted_at) < current_date;"
    )
    print("Delete saturacao past")
    eng_master_conn.close()


def refresh_mvw_orders_past():
    eng_master_conn = eng_master()
    eng_master_conn.execute(
        "refresh materialized view concurrently  mvw_liveops_orders_past;"
    )
    print("Refresh orders past done")
    eng_master_conn.close()


def refresh_mvw_status_past():
    eng_master_conn = eng_master()
    eng_master_conn.execute(
        "refresh materialized view concurrently  mvw_liveops_status_histories_past;"
    )
    print("Refresh Status Histories past done")
    eng_master_conn.close()


def refresh_mvw_drivers_past():
    eng_master_conn = eng_master()
    eng_master_conn.execute(
        "refresh materialized view concurrently  mvw_liveops_drivers_past;"
    )
    print("Refresh Status Histories past done")
    eng_master_conn.close()


def run():
    run_delete_old_orders()
    run_delete_old_drivers()
    run_delete_old_saturacao()
    refresh_mvw_orders_past()
    refresh_mvw_status_past()
    refresh_mvw_drivers_past()


if __name__ == "__main__":

    run()
