def query_orders_inc(creation_max):
    return """
            select distinct
            oh.order_id,
            date_trunc('second', timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo') as status_created_at,
            date_trunc('second', timezone('UTC'::text, o.created_at) at time zone 'America/Sao_paulo') as order_created_at,
            date_trunc('MILLISECONDS', timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo') as order_created_at_ms,
            date_trunc('MILLISECONDS'::text,
                        timezone('America/Sao_Paulo'::text, current_timestamp)) as last_updated,
            UPPER(oh.status) as status_dinamico,
            case when UPPER(oh.status)='CREATED'       then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as created_created_at,
            case when UPPER(oh.status)='ACCEPTED'      then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as accepted_created_at,
            case when UPPER(oh.status)='READY'         then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as ready_created_at,
            case when UPPER(oh.status)='COLLECTED'     then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as collected_created_at,
            case when UPPER(oh.status)='IN_EXPEDITION' then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as in_expedition_created_at,
            case when UPPER(oh.status)='DELIVERING'    then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as delivering_created_at,
            case when UPPER(oh.status)='DELIVERED'     then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as delivered_created_at,
            case when UPPER(oh.status)='FINISHED'      then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as finished_created_at,
            op."name" AS hub_name,
            op.id as hub_id,
            s.segment,
            s.id as store_id,
            split_part(s.name::text, '-'::text, 1) AS store_name,
            a.state,
            a.city,
            deliveries.driver_id,
            o.pickup
        FROM order_histories as oh
            JOIN orders o on o.id = oh.order_id
            JOIN stores s ON s.id = o.store_id
            JOIN operation_centers op ON op.id = s.operation_center_id
            LEFT JOIN deliveries ON o.id = deliveries.order_id
            LEFT JOIN addresses a ON a.source_id = op.id AND a.source_type::text = 'OperationCenter'::text
        WHERE  date_trunc('MILLISECONDS', timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo') > '{creation_max}' and 
        op.id  not in (32,14,45,7,6) and op.deleted_at is null
        AND  a.deleted_at is null
    """.format(
        creation_max=creation_max
    )


def query_orders_inc_past():
    return """
    select distinct
            oh.order_id,
            date_trunc('second', timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo') as status_created_at,
            date_trunc('second', timezone('UTC'::text, o.created_at) at time zone 'America/Sao_paulo') as order_created_at,
            date_trunc('MILLISECONDS', timezone('UTC'::text, o.created_at) at time zone 'America/Sao_paulo') as order_created_at_ms,
            date_trunc('MILLISECONDS'::text,
                        timezone('America/Sao_Paulo'::text, current_timestamp)) as last_updated,
            UPPER(oh.status) as status_dinamico,
            case when UPPER(oh.status)='CREATED'       then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as created_created_at,
            case when UPPER(oh.status)='ACCEPTED'      then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as accepted_created_at,
            case when UPPER(oh.status)='READY'         then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as ready_created_at,
            case when UPPER(oh.status)='COLLECTED'     then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as collected_created_at,
            case when UPPER(oh.status)='IN_EXPEDITION' then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as in_expedition_created_at,
            case when UPPER(oh.status)='DELIVERING'    then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as delivering_created_at,
            case when UPPER(oh.status)='DELIVERED'     then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as delivered_created_at,
            case when UPPER(oh.status)='FINISHED'      then timezone('UTC'::text, oh.created_at) at time zone 'America/Sao_paulo' end as finished_created_at,
            op."name" AS hub_name,
            op.id as hub_id,
            s.segment,
            s.id as store_id,
            split_part(s.name::text, '-'::text, 1) AS store_name,
            a.state,
            a.city,
            deliveries.driver_id,
            o.pickup
        FROM order_histories as oh
            JOIN orders o on o.id = oh.order_id
            JOIN stores s ON s.id = o.store_id
            JOIN operation_centers op ON op.id = s.operation_center_id
            LEFT JOIN deliveries ON o.id = deliveries.order_id
            LEFT JOIN addresses a ON a.source_id = op.id AND a.source_type::text = 'OperationCenter'::text
        WHERE date(timezone('UTC'::text, o.created_at) at time zone 'America/Sao_paulo') >= ''
        and date(timezone('UTC'::text, o.created_at) at time zone 'America/Sao_paulo') <= ''
        op.id  not in (32,14,45,7,6) or op.deleted_at is null
        AND  a.deleted_at is null
    order by date_trunc('MILLISECONDS', timezone('UTC'::text, o.created_at) at time zone 'America/Sao_paulo')  asc;
    """


def query_drivers(creation_max):
    return """
         WITH stations AS (
    SELECT a_1.source_id AS station_id,
           oc.name       AS station_name,
           a_1.latitude,
           a_1.longitude,
           a_1.city,
           a_1.state
    FROM addresses a_1
             JOIN operation_centers oc ON oc.id = a_1.source_id AND a_1.source_type::text = 'OperationCenter'::text
    WHERE a_1.deleted_at IS NULL
      AND (a_1.latitude IS NOT NULL OR a_1.longitude IS NOT NULL)
      AND oc.name::text !~~* '%teste%'::text
      AND oc.name::text !~~* '%treinamento%'::text
),
     driver_opcenter AS (
         SELECT DISTINCT ON (dh_1.driver_id) dh_1.driver_id,
                                             CASE
                                                 WHEN st.station_id IS NULL THEN 0
                                                 ELSE st.station_id
                                                 END AS station_id,
                                             CASE
                                                 WHEN st.station_name IS NULL
                                                     THEN 'Imprecisão da Bringg'::character varying
                                                 ELSE st.station_name
                                                 END AS station_name,
                                             st.city,
                                             st.state
         FROM driver_histories dh_1
                  LEFT JOIN stations st ON st_distancesphere(st_point(dh_1.latitude::double precision,
                                                                      dh_1.longitude::double precision),
                                                             st_point(st.latitude::double precision,
                                                                      st.longitude::double precision)) <=
                                           500::double precision
         WHERE date(timezone('UTC'::text, dh_1.created_at)) = CURRENT_DATE
           AND dh_1.at_home = true
         ORDER BY dh_1.driver_id, dh_1.created_at DESC
     )
SELECT DISTINCT date_trunc('MILLISECONDS'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')  AS driver_creation_ms,
                date_trunc('minute'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')       AS driver_update,
                date_part('hour'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')           AS driver_update_hour,
                date_part('minute'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')         AS driver_update_minute,
                dh.driver_id,
                CASE
                    WHEN dr.driver_type_id = 1 THEN 'Motoboy'::text
                    WHEN dr.driver_type_id = 2 THEN 'Biker'::text
                    ELSE 'Verificar'::text
                    END                                                          AS driver_type,
                lo.independent,
                dh.status,
                dh.at_home,
                CASE
                    WHEN doc.station_id IS NULL THEN 0
                    ELSE doc.station_id
                    END                                                          AS station_id,
                CASE
                    WHEN doc.station_name IS NULL THEN 'Imprecisão da Bringg'::character varying
                    ELSE doc.station_name
                    END                                                          AS station_name,
                CASE
                    WHEN doc.city IS NULL AND a.city::text = ''::text THEN 'Cadastro Incompleto'::character varying
                    ELSE COALESCE(doc.city, a.city)
                    END                                                          AS city,
                CASE
                    WHEN doc.state IS NULL AND a.state::text = ''::text THEN 'Cadastro Incompleto'::character varying
                    ELSE COALESCE(doc.state, a.state)
                    END                                                          AS state
FROM driver_histories dh
         JOIN drivers dr ON dr.id = dh.driver_id
         LEFT JOIN logistics_operators lo ON lo.id = dr.logistics_operator_id
         LEFT JOIN driver_opcenter doc ON doc.driver_id = dh.driver_id
         LEFT JOIN addresses a ON dr.id = a.source_id AND a.source_type::text = 'Driver'::text
    WHERE date_trunc('MILLISECONDS'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo') >  '{creation_max}';
        """.format(
        creation_max=creation_max
    )


def query_drivers_past():
    return """
        WITH stations AS (
    SELECT a_1.source_id AS station_id,
           oc.name       AS station_name,
           a_1.latitude,
           a_1.longitude,
           a_1.city,
           a_1.state
    FROM addresses a_1
             JOIN operation_centers oc ON oc.id = a_1.source_id AND a_1.source_type::text = 'OperationCenter'::text
    WHERE a_1.deleted_at IS NULL
      AND (a_1.latitude IS NOT NULL OR a_1.longitude IS NOT NULL)
      AND oc.name::text !~~* '%teste%'::text
      AND oc.name::text !~~* '%treinamento%'::text
),
     driver_opcenter AS (
         SELECT DISTINCT ON (dh_1.driver_id) dh_1.driver_id,
                                             CASE
                                                 WHEN st.station_id IS NULL THEN 0
                                                 ELSE st.station_id
                                                 END AS station_id,
                                             CASE
                                                 WHEN st.station_name IS NULL
                                                     THEN 'Imprecisão da Bringg'::character varying
                                                 ELSE st.station_name
                                                 END AS station_name,
                                             st.city,
                                             st.state
         FROM driver_histories dh_1
                  LEFT JOIN stations st ON st_distancesphere(st_point(dh_1.latitude::double precision,
                                                                      dh_1.longitude::double precision),
                                                             st_point(st.latitude::double precision,
                                                                      st.longitude::double precision)) <=
                                           500::double precision
         WHERE date(timezone('UTC'::text, dh_1.created_at)) = CURRENT_DATE
           AND dh_1.at_home = true
         ORDER BY dh_1.driver_id, dh_1.created_at DESC
     )
SELECT DISTINCT date_trunc('MILLISECONDS'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')  AS driver_creation_ms,
                date_trunc('minute'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')       AS driver_update,
                date_part('hour'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')           AS driver_update_hour,
                date_part('minute'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')         AS driver_update_minute,
                dh.driver_id,
                CASE
                    WHEN dr.driver_type_id = 1 THEN 'Motoboy'::text
                    WHEN dr.driver_type_id = 2 THEN 'Biker'::text
                    ELSE 'Verificar'::text
                    END                                                          AS driver_type,
                lo.independent,
                dh.status,
                dh.at_home,
                CASE
                    WHEN doc.station_id IS NULL THEN 0
                    ELSE doc.station_id
                    END                                                          AS station_id,
                CASE
                    WHEN doc.station_name IS NULL THEN 'Imprecisão da Bringg'::character varying
                    ELSE doc.station_name
                    END                                                          AS station_name,
                CASE
                    WHEN doc.city IS NULL AND a.city::text = ''::text THEN 'Cadastro Incompleto'::character varying
                    ELSE COALESCE(doc.city, a.city)
                    END                                                          AS city,
                CASE
                    WHEN doc.state IS NULL AND a.state::text = ''::text THEN 'Cadastro Incompleto'::character varying
                    ELSE COALESCE(doc.state, a.state)
                    END                                                          AS state
FROM driver_histories dh
         JOIN drivers dr ON dr.id = dh.driver_id
         LEFT JOIN logistics_operators lo ON lo.id = dr.logistics_operator_id
         LEFT JOIN driver_opcenter doc ON doc.driver_id = dh.driver_id
         LEFT JOIN addresses a ON dr.id = a.source_id AND a.source_type::text = 'Driver'::text
    WHERE date_trunc('MILLISECONDS'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo') >=  ''
    and  date_trunc('MILLISECONDS'::text, timezone('UTC'::text, dh.created_at)at time zone  'America/Sao_paulo')  <=   ''

        """


def query_apinova(creation_max):
    return """
        select hub_orders.id::varchar                        as hub_orders_id,
        hub_stores.name                                   as store_name,
        hub_stores.segment                                as segmento,
        hub_deliveries.order_retry_reason_id              as banda_extra,
        hub_deliveries.freight_price                      as freight_deliveries_price,
            description as motivo_banda_extra,
            order_retry_type as tipo_banda_extra
        from public.orders as hub_orders
            left join public.stores as hub_stores on hub_stores.id = hub_orders.store_id
            left join public.deliveries as hub_deliveries on hub_orders.id = hub_deliveries.order_id
            left join order_retry_reasons on order_retry_reason_id= order_retry_reasons.id
    WHERE  date_trunc('milliseconds', timezone('UTC', hub_deliveries.created_at)) 
        AT TIME ZONE 'America/Sao_Paulo' >
            '{creation_max}'
        and hub_stores.segment = 'FOODS'  
        """.format(
        creation_max=creation_max
    )


def query_tuktuk(creation_max):
    return """
         WITH base AS (
        SELECT orders.race_id                                                                         AS races_id,
            orders.external_id,
            row_number() OVER (PARTITION BY orders.race_id ORDER BY (max(orders.way_inserted_at))) AS seq_delivered,
            count(DISTINCT orders.external_id)                                                     AS total_pedidos,
            string_agg(DISTINCT orders.way_inserted_at::character varying::text, ','::text)        AS way_points,
            max(orders.inserted_at)                                                                AS inserted_at,
            max(orders.hub_name::text)                                                             AS hub_name,
            max(orders.route_distance_in_meters)                                                   AS route_distance_in_meters,
            max(orders.freight_races)                                                              AS freight_races,
            max(orders.position_1::character varying::text)::point                                 AS position_hub,
            CASE
                WHEN row_number() OVER (PARTITION BY orders.race_id ORDER BY (max(orders.way_inserted_at))) = 1
                    THEN max(orders.position_2::character varying::text)::point
                ELSE NULL::point
                END                                                                                AS pont_1,
            CASE
                WHEN row_number() OVER (PARTITION BY orders.race_id ORDER BY (max(orders.way_inserted_at))) = 2
                    THEN max(orders.position_2::character varying::text)::point
                ELSE NULL::point
                END                                                                                AS pont_1_2,
            CASE
                WHEN row_number() OVER (PARTITION BY orders.race_id ORDER BY (max(orders.way_inserted_at))) = 3
                    THEN max(orders.position_2::character varying::text)::point
                ELSE NULL::point
                END                                                                                AS pont_2_3,
            CASE
                WHEN row_number() OVER (PARTITION BY orders.race_id ORDER BY (max(orders.way_inserted_at))) = 4
                    THEN max(orders.position_2::character varying::text)::point
                ELSE NULL::point
                END                                                                                AS pont_3_4,
            CASE
                WHEN row_number() OVER (PARTITION BY orders.race_id ORDER BY (max(orders.way_inserted_at))) = 5
                    THEN max(orders.position_2::character varying::text)::point
                ELSE NULL::point
                END                                                                                AS pont_4_5,
            CASE
                WHEN row_number() OVER (PARTITION BY orders.race_id ORDER BY (max(orders.way_inserted_at))) = 6
                    THEN max(orders.position_2::character varying::text)::point
                ELSE NULL::point
                END                                                                                AS pont_5_6,
            CASE
                WHEN row_number() OVER (PARTITION BY orders.race_id ORDER BY (max(orders.way_inserted_at))) = 7
                    THEN max(orders.position_2::character varying::text)::point
                ELSE NULL::point
                END                                                                                AS pont_6_7,
            CASE
                WHEN row_number() OVER (PARTITION BY orders.race_id ORDER BY (max(orders.way_inserted_at))) = 8
                    THEN max(orders.position_2::character varying::text)::point
                ELSE NULL::point
                END                                                                                AS pont_7_8,
            CASE
                WHEN row_number() OVER (PARTITION BY orders.race_id ORDER BY (max(orders.way_inserted_at))) = 9
                    THEN max(orders.position_2::character varying::text)::point
                ELSE NULL::point
                END                                                                                AS pont_8_9,
            max(orders.position_2::character varying::text)::point                                 AS position_2,
            max(orders.position_3::character varying::text)::point                                 AS position_3
        FROM (SELECT orders_1.id                AS tuktuk_delivery_id,
                    races_1.id                 AS race_id,
                    orders_1.external_id,
                    races_1.inserted_at,
                    oc.name                    AS hub_name,
                    races_1.route_distance_in_meters,
                    races_1.freight_total_rate AS freight_races,
                    CASE
                        WHEN way."position" = 1 THEN way.coordinates
                        ELSE NULL::point
                        END                    AS position_1,
                    CASE
                        WHEN way."position" = 2 THEN way.coordinates
                        ELSE NULL::point
                        END                    AS position_2,
                    CASE
                        WHEN way."position" = 3 THEN way.coordinates
                        ELSE NULL::point
                        END                    AS position_3,
                    way.inserted_at            AS way_inserted_at
            FROM    orders orders_1
                    LEFT JOIN waypoints way ON orders_1.id = way.order_id
                    LEFT JOIN races races_1 ON orders_1.race_id = races_1.id
                    LEFT JOIN operation_centers oc ON oc.id = orders_1.operation_center_id
            WHERE orders_1.external_id IS NOT NULL
                AND races_1.id IS NOT NULL
                AND races_1.route_distance_in_meters >= 10000
                AND races_1.freight_total_rate > 10::numeric
                AND races_1.freight_total_rate IS NOT NULL) orders
        GROUP BY orders.race_id, orders.external_id
        HAVING count(orders.external_id) > 1
    ),
        races AS (
            SELECT base_1.races_id,
                    max(base_1.seq_delivered)                       AS seq_delivered,
                    string_agg(base_1.external_id::text, ','::text) AS pedidos_agg,
                    count(base_1.external_id)                       AS total_pedidos,
                    base_1.external_id                              AS pedidos
            FROM base base_1
            GROUP BY base_1.races_id, base_1.external_id
        ),
        deliveries AS (
            SELECT base_1.races_id,
                    max(base_1.hub_name)                                     AS hub_name,
                    max(base_1.route_distance_in_meters)                     AS route_distance_in_meters,
                    max(base_1.freight_races)                                AS freight_races,
                    max(base_1.position_hub::character varying::text)::point AS position_hub,
                    max(base_1.pont_1::character varying::text)::point       AS pont_1,
                    max(base_1.pont_1_2::character varying::text)::point     AS pont_1_2,
                    max(base_1.pont_2_3::character varying::text)::point     AS pont_2_3,
                    max(base_1.pont_3_4::character varying::text)::point     AS pont_3_4,
                    max(base_1.pont_4_5::character varying::text)::point     AS pont_4_5,
                    max(base_1.pont_5_6::character varying::text)::point     AS pont_5_6,
                    max(base_1.pont_6_7::character varying::text)::point     AS pont_6_7,
                    max(base_1.pont_7_8::character varying::text)::point     AS pont_7_8,
                    max(base_1.pont_8_9::character varying::text)::point     AS pont_8_9,
                    max(base_1.inserted_at)                                  AS inserted_at
            FROM base base_1
            GROUP BY base_1.races_id
        ),
        coordenadas AS (
            SELECT deliveries_1.races_id,
                    concat('https://www.google.com.br/maps/dir/', deliveries_1.position_hub::character varying::point, '/',
                        deliveries_1.pont_1::character varying::point)                                                 AS link_rota_1,
                    CASE
                        WHEN deliveries_1.pont_1_2 IS NOT NULL THEN concat('https://www.google.com.br/maps/dir/',
                                                                        deliveries_1.pont_1::character varying::point,
                                                                        '/',
                                                                        deliveries_1.pont_1_2::character varying::point)
                        ELSE NULL::text
                        END                                                                                               AS link_rota_2,
                    CASE
                        WHEN deliveries_1.pont_2_3 IS NOT NULL THEN concat('https://www.google.com.br/maps/dir/',
                                                                        deliveries_1.pont_1_2::character varying::point,
                                                                        '/',
                                                                        deliveries_1.pont_2_3::character varying::point)
                        ELSE NULL::text
                        END                                                                                               AS link_rota_3,
                    CASE
                        WHEN deliveries_1.pont_3_4 IS NOT NULL THEN concat('https://www.google.com.br/maps/dir/',
                                                                        deliveries_1.pont_2_3::character varying::point,
                                                                        '/',
                                                                        deliveries_1.pont_3_4::character varying::point)
                        ELSE NULL::text
                        END                                                                                               AS link_rota_4,
                    CASE
                        WHEN deliveries_1.pont_4_5 IS NOT NULL THEN concat('https://www.google.com.br/maps/dir/',
                                                                        deliveries_1.pont_3_4::character varying::point,
                                                                        '/',
                                                                        deliveries_1.pont_4_5::character varying::point)
                        ELSE NULL::text
                        END                                                                                               AS link_rota_5,
                    CASE
                        WHEN deliveries_1.pont_5_6 IS NOT NULL THEN concat('https://www.google.com.br/maps/dir/',
                                                                        deliveries_1.pont_4_5::character varying::point,
                                                                        '/',
                                                                        deliveries_1.pont_5_6::character varying::point)
                        ELSE NULL::text
                        END                                                                                               AS link_rota_6,
                    CASE
                        WHEN deliveries_1.pont_6_7 IS NOT NULL THEN concat('https://www.google.com.br/maps/dir/',
                                                                        deliveries_1.pont_5_6::character varying::point,
                                                                        '/',
                                                                        deliveries_1.pont_6_7::character varying::point)
                        ELSE NULL::text
                        END                                                                                               AS link_rota_7,
                    CASE
                        WHEN deliveries_1.pont_7_8 IS NOT NULL THEN concat('https://www.google.com.br/maps/dir/',
                                                                        deliveries_1.pont_6_7::character varying::point,
                                                                        '/',
                                                                        deliveries_1.pont_7_8::character varying::point)
                        ELSE NULL::text
                        END                                                                                               AS link_rota_8,
                    CASE
                        WHEN deliveries_1.pont_8_9 IS NOT NULL THEN concat('https://www.google.com.br/maps/dir/',
                                                                        deliveries_1.pont_7_8::character varying::point,
                                                                        '/',
                                                                        deliveries_1.pont_8_9::character varying::point)
                        ELSE NULL::text
                        END                                                                                               AS link_rota_9,
                    concat_ws('/'::text, 'https://www.google.com.br/maps/dir', deliveries_1.position_hub,
                            deliveries_1.pont_1, deliveries_1.pont_1_2, deliveries_1.pont_2_3, deliveries_1.pont_3_4,
                            deliveries_1.pont_4_5, deliveries_1.pont_5_6, deliveries_1.pont_6_7,
                            deliveries_1.pont_7_8)                                                                      AS link_rota_completa
            FROM deliveries deliveries_1
        )
    SELECT races.pedidos,
        races.races_id,
        max(races.seq_delivered)                                                                 AS ordem_casamento,
        deliveries.hub_name,
        deliveries.route_distance_in_meters,
        deliveries.freight_races,
        timezone('america/Sao_Paulo'::text, timezone('utc'::text, deliveries.inserted_at))::text AS inserted_at,
        date_trunc('MILLISECONDS',timezone('utc'::text, deliveries.inserted_at))AS insert_formart_ms,
        coordenadas.link_rota_completa
    FROM deliveries
            JOIN base ON base.races_id = base.races_id
            JOIN races ON races.races_id = deliveries.races_id
            JOIN coordenadas ON races.races_id = coordenadas.races_id
        
    WHERE date_trunc('MILLISECONDS',timezone('utc'::text, deliveries.inserted_at)) >  
    '{creation_max}'
    GROUP BY races.races_id, races.pedidos, races.pedidos_agg, deliveries.hub_name, deliveries.route_distance_in_meters,
            deliveries.freight_races, deliveries.inserted_at, (date(timezone('UTC'::text, deliveries.inserted_at))::text),
            coordenadas.link_rota_completa
    ORDER BY races.races_id;
     """.format(
        creation_max=creation_max
    )