from dao import *
from querys_incremen import *
import time
from sqlalchemy import create_engine

bol_day = True


def eng_master():
    conn = engine_master()
    eng_master = conn.connect()
    return eng_master


def run_api_nova(creation_max):
    df_apinova = query_get_replica(query_apinova(str(creation_max[0][0])))
    if len(df_apinova) > 0:
        df_apinova.reset_index(drop=True, inplace=True)
        query_insert_master(dataframe=df_apinova, table_destiny="liveops_api_nova")
        print("ApiNova - News orders with  hight freight:", creation_max)
    else:
        print("ApiNova - No news orders with hight freight:", creation_max)


def run_tuktuk(creation_max):
    df_tuktuk = query_get_replica_tuktuk(query_tuktuk(str(creation_max[0][0])))
    if len(df_tuktuk) > 0:
        df_tuktuk.reset_index(drop=True, inplace=True)
        query_insert_master(dataframe=df_tuktuk, table_destiny="liveops_tuktuk")
        print("TukTuk - News orders with  hight freight:", creation_max)
    else:
        print("TukTuk -  No news orders with hight freight:", creation_max)


def run():
    global bol_day
    eng_master_conn_apinova = eng_master()
    creation_date = eng_master_conn_apinova.execute(
        "select date(current_timestamp at time zone 'America/Sao_Paulo');"
    ).fetchall()
    eng_master_conn_apinova.close()
    while bol_day == True:
        eng_master_conn = eng_master()
        creation_max_apinova = eng_master_conn.execute(
            "select max(timezone('UTC'::text, deliveries_createad_ms at time zone  'America/Sao_paulo')) from liveops_api_nova;"
        ).fetchall()
        creation_max_tuktuk = eng_master_conn.execute(
            "select max(timezone('UTC'::text, insert_formart_ms at time zone  'America/Sao_paulo')) from liveops_tuktuk;"
        ).fetchall()
        run_api_nova(creation_max_apinova)
        time.sleep(3)
        run_tuktuk(creation_max_tuktuk)
        eng_master_conn.execute(
            "refresh materialized view concurrently mvw_liveops_monitor_frete;"
        )
        print("Updated View Sucess!")
        time.sleep(3)
        same_day = eng_master_conn.execute(
            "select date(current_timestamp at time zone 'America/Sao_Paulo');"
        ).fetchall()
        if creation_date == same_day:
            eng_master_conn.close()
            time.sleep(120)
            bol_day = True
        else:
            print("Encerrando o dia  - Monitor de Frete")
            eng_master_conn.close()
            bol_day = False


if __name__ == "__main__":

    run()
