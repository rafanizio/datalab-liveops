def refresh_mvw_orders():
    return """ 
            refresh materialized view concurrently  public.mvw_liveops_orders;
    """


def refresh_mvw_orders_past():
    return """
            refresh materialized view concurrently  public.mvw_liveops_orders_past;
    """


def refresh_mvw_drivers():
    return """
            refresh materialized view concurrently  public.mvw_liveops_drivers;
    """


def refresh_mvw_drivers_past():
    return """
           refresh materialized view concurrently  public.mvw_liveops_drivers_past;
    """


def refresh_mvw_status_histories():
    return """
           refresh materialized view concurrently  public.mvw_liveops_status_histories;
    """


def refresh_mvw_status_histories_past():
    return """
           refresh materialized view concurrently  public.mvw_liveops_status_histories_past;
    """