import psycopg2
import time
import csv


def split_sql():
    splitLen = 4000
    outputBase = "liveops"
    input = open("liveops.sql", "r").read().split(";")
    at = 1
    for lines in range(0, len(input), splitLen):
        outputData = input[lines : lines + splitLen]
        output = open(outputBase + str(at) + ".sql", "w")
        output.write(";".join(outputData))
        output.close()
        at += 1


def run():
    split_sql()


if __name__ == "__main__":

    run()
